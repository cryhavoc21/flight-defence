#include "stdafx.h"

#ifndef ENEMY_H_
#define ENEMY_H_

class Enemy : public Entity {
public:
	Enemy() {}
	Enemy(D3DXVECTOR2 location, Entity**, Entity**, uint8_t*);
	~Enemy();

	void init_mesh(LPDIRECT3DDEVICE9 d3ddev, Model**);
	void init_sprite(LPDIRECT3DDEVICE9 a, LPD3DXSPRITE spritedev, TSprite**);
	void Release();
	void draw_3d();
	void draw_2d();
	void update_3d();
	void update_2d();

private:
	D3DXVECTOR3 thrust{ 0.0f, 0.0f, -20.0f };
	D3DXVECTOR3 lift{ 0.0f, 9.8f, 0.0f };

	D3DXVECTOR3 up_vector = { 0.0f, 1.0f, 0.0f };

	PhysBody *phys_body = new PhysBody(GRAV_ENABLED);

	D3DXVECTOR3 velocity{ 0.0f, 0.0f, 0.0f };
	D3DXVECTOR3 location{ 0.0f, 0.0f, 0.0f };

	void update_physics();
	float engage_radius_inner = 64.0f;
	TSprite *engage_circle;
};

#endif //ENEMY_H_
