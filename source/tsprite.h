#ifndef TSPRITE_H_
#define TSPRITE_H_

class TSprite {
public:
	TSprite() {}
	TSprite(LPDIRECT3DDEVICE9 a, LPD3DXSPRITE b, LPCWSTR path, int w, int h);
	~TSprite() {}

	void transform(D3DXVECTOR2 loc, float angle, D3DXVECTOR2 scl);
	void draw();
	void Release();
	int width;
	int height;

private:
	D3DXMATRIX tMatrix;
	LPD3DXSPRITE spritedev;
	LPDIRECT3DDEVICE9 d3ddev;
	LPDIRECT3DTEXTURE9 texture;
};

#endif //TEXTURE_H_