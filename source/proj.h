#include "stdafx.h"

#ifndef PROJECTILE_H_
#define PROJECTILE_H_

class Projectile : public Entity
{
public:
	Projectile();
	Projectile(D3DXVECTOR3 playerLocation, float fireYawAngle, float firePitchAngle);
	~Projectile();
	void init_mesh(LPDIRECT3DDEVICE9 a, Model** model_list);
	void init_sprite(LPDIRECT3DDEVICE9, LPD3DXSPRITE, TSprite**) {}
	void update_2d() {}
	void update_3d();
	void draw_2d() {}
	void draw_3d();
	void Release();

	float velocity;
	float yaw;
	float pitch;
};

#endif //PROJECTILE_H_