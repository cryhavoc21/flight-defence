#include "stdafx.h"
#include "skybox.h"

Skybox::Skybox(LPDIRECT3DDEVICE9 a) {
	d3ddev = a;

	if (FAILED(D3DXCreateTextureFromFile(d3ddev, L"media\\backgroundDefault.png", &texture)))
	{
		// If texture is not in current folder, try parent folder
		if (FAILED(D3DXCreateTextureFromFile(d3ddev, L"..\\media\\backgroundDefault.png", &texture)))
		{
			printf("Failed to find texture.\n");
		}
	}

	static const float cube_len = 1.0f;
	static const uint8_t num_vertices = 24;
	CUSTOM_TEXTURE_VERTEX vertices[] = {
		{ -cube_len, -cube_len, cube_len, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f },    // side 1
		{ cube_len, -cube_len, cube_len, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f },
		{ -cube_len, cube_len, cube_len, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f },
		{ cube_len, cube_len, cube_len, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f },

		{ -cube_len, -cube_len, -cube_len, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },    // side 2
		{ -cube_len, cube_len, -cube_len, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },
		{ cube_len, -cube_len, -cube_len, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },
		{ cube_len, cube_len, -cube_len, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },

		{ -cube_len, cube_len, -cube_len, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f },    // Top
		{ -cube_len, cube_len, cube_len, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f },
		{ cube_len, cube_len, -cube_len, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f },
		{ cube_len, cube_len, cube_len, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f },

		{ -cube_len, -cube_len, -cube_len, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f },    // Bottom
		{ cube_len, -cube_len, -cube_len, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f },
		{ -cube_len, -cube_len, cube_len, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f },
		{ cube_len, -cube_len, cube_len, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f },

		{ cube_len, -cube_len, -cube_len, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f },    // side 5
		{ cube_len, cube_len, -cube_len, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ cube_len, -cube_len, cube_len, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ cube_len, cube_len, cube_len, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f },

		{ -cube_len, -cube_len, -cube_len, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f },    // side 6
		{ -cube_len, -cube_len, cube_len, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ -cube_len, cube_len, -cube_len, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f },
		{ -cube_len, cube_len, cube_len, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f },
	};

	short indices[] = {
		2, 1, 0,
		3, 1, 2,
		6, 5, 4,
		7, 5, 6,
		10, 9, 8,
		11, 9, 10,
		14, 13, 12,
		15, 13, 14,
		18, 17, 16,
		19, 17, 18,
		22, 21, 20,
		23, 21, 22
	};

	if (FAILED(d3ddev->CreateVertexBuffer(num_vertices * sizeof(CUSTOM_COLOR_VERTEX),
		D3DUSAGE_WRITEONLY,
		CUSTOM_COLOR_FVF,
		D3DPOOL_MANAGED,
		&vert_buffer,
		NULL))) printf("Vertex Buffered Failed\n");

	if (FAILED(d3ddev->CreateIndexBuffer(sizeof(indices),
		0,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&index_buffer,
		NULL))) printf("Index Buffer Failed\n");

	VOID* pVoid;

	vert_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	vert_buffer->Unlock();

	index_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, indices, sizeof(indices));
	index_buffer->Unlock();
}

void Skybox::draw() {
	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	d3ddev->SetTexture(0, texture);
	//d3ddev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	//d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	//d3ddev->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	//d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_DISABLE);

	d3ddev->SetFVF(CUSTOM_TEX_FVF);

	d3ddev->SetStreamSource(0, vert_buffer, 0, sizeof(CUSTOM_TEXTURE_VERTEX));
	d3ddev->SetIndices(index_buffer);
	d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 24, 0, 12);

	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}

void Skybox::Release() {
	//vert_buffer->Release();
}

Skybox::~Skybox() {

}
