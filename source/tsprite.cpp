#include "stdafx.h"
#include "tsprite.h"

TSprite::TSprite(LPDIRECT3DDEVICE9 a, LPD3DXSPRITE b, LPCWSTR path, int w, int h) {
	d3ddev = a;
	spritedev = b;
	//printf("path is %s\n", (char*)path);
	if (FAILED(D3DXCreateTextureFromFile(d3ddev, path, &texture))) {
		const CHAR* strPrefix = "..\\";
		CHAR strModel[MAX_PATH];
		strcpy_s(strModel, MAX_PATH, strPrefix);
		strcat_s(strModel, MAX_PATH, (char*)path);
		wprintf(L"%s path %s\n", (LPCWSTR)strModel, (LPCWSTR)path);
		if (FAILED(D3DXCreateTextureFromFile(d3ddev, (LPCWSTR)strModel, &texture))) {
			wprintf(L"Texture at path %s failed to load.\n", path);
		}
	}

	width = w;
	height = h;
}

void TSprite::draw() {
	spritedev->SetTransform(&tMatrix);
	spritedev->Draw(texture, NULL, NULL, NULL, 0xFFFFFFFF); // 0xFFFFFFFF maintains the coloration of the sprite
}

void TSprite::transform(D3DXVECTOR2 loc, float angle, D3DXVECTOR2 scl) {
	D3DXVECTOR2 center{ (float)(width / 2) * scl.x, (float)(height / 2) * scl.y };
	D3DXMatrixTransformation2D(&tMatrix, NULL, NULL, &scl, &center, angle, &loc);
}

void TSprite::Release() {
	printf("texture destruction\n");
	if (texture) { texture->Release(); }
}