// Utility functions for vectors

#include "../stdafx.h"

#ifndef UTIL_VECT_H_
#define UTIL_VECT_H_

inline void printf_vector_3d(D3DXVECTOR3 vect) {
	printf("x = %.2f; y = %.2f; z = %.2f;\n", vect.x, vect.y, vect.z);
}
inline void printf_vector_2d(D3DXVECTOR2 vect) {
	printf("x = %.2f; y = %.2f;\n", vect.x, vect.y);
}

inline float distance_2d(D3DXVECTOR2 *a, D3DXVECTOR2 *b) {
	return sqrt(pow((b->x - a->x), 2) + pow((b->y - a->y), 2));
}

inline void transform(LPDIRECT3DDEVICE9 d3ddev, D3DXVECTOR3 loc, D3DXVECTOR3 rot, D3DXVECTOR3 scl) {
	// a matrix to store the rotation information
	D3DXMATRIX matRotate, matRotateX, matRotateY, matRotateZ, matTrans, matScale;

	D3DXMatrixIdentity(&matTrans);
	D3DXMatrixIdentity(&matScale);
	D3DXMatrixIdentity(&matRotate);

	// build a matrix to rotate the model based on the increasing float value
	D3DXMatrixRotationX(&matRotateX, rot.x);
	D3DXMatrixRotationY(&matRotateY, rot.y);
	D3DXMatrixRotationZ(&matRotateZ, rot.z);
	D3DXMatrixTranslation(&matTrans, loc.x, loc.y, loc.z);
	D3DXMatrixScaling(&matScale, scl.x, scl.y, scl.z);

	matRotate = matRotateZ * matRotateX * matRotateY;

	d3ddev->SetTransform(D3DTS_WORLD, &(matRotate * matScale * matTrans));
}

#endif //UTIL_VECT_H_