// Utility functions to be used by the renderer device

#include "../stdafx.h"

#ifndef UTIL_REND_H_
#define UTIL_REND_H_

const float NEAR_VIEW = 0.1f;
const float FAR_VIEW = 5000.0f;

inline void position_camera(LPDIRECT3DDEVICE9 d3ddev, Camera *cam) {
	D3DXMATRIX matView;    // the view transform matrix
	D3DXMatrixLookAtLH(&matView, &(cam->loc), &(cam->look), &(cam->up));
	// set the view transform to matView
	d3ddev->SetTransform(D3DTS_VIEW, &matView);

	D3DXMATRIX matProjection;     // the projection transform matrix
	D3DXMatrixPerspectiveFovLH(&matProjection,
		D3DXToRadian(75),    // the horizontal field of view
		(FLOAT)SCREEN_WIDTH / (FLOAT)SCREEN_HEIGHT, // aspect ratio
		NEAR_VIEW,    // the near view-plane
		FAR_VIEW);    // the far view-plane
	d3ddev->SetTransform(D3DTS_PROJECTION, &matProjection);
}

inline HRESULT load_font(LPDIRECT3DDEVICE9 d3ddev, LPD3DXFONT *font, LPCWSTR name) {
	D3DXFONT_DESC font_info;

	ZeroMemory(&font_info, sizeof(D3DXFONT_DESC));

	font_info.Height = 50;
	font_info.Width = 0;
	font_info.Weight = 500;
	font_info.MipLevels = D3DX_DEFAULT;
	font_info.Italic = false;
	font_info.CharSet = 0;
	font_info.OutputPrecision = 0;
	font_info.Quality = 0;
	font_info.PitchAndFamily = 0;
	wcscpy(font_info.FaceName, name);
	
	return D3DXCreateFontIndirect(d3ddev, &font_info, &(*font));
}

#endif // UTIL_REND_H_
