#include "stdafx.h"
#include "player.h"

Player::Player(D3DXVECTOR2 location, int *k, uint8_t *flags, HWND hWnd, Entity** list) {
	swingcam = new Camera();
	swingcam->loc = loc_3d;
	swingcam->up = { 0.0f, 1.0f, 0.0f };

	scl_3d = { 0.5f, 0.5f, 0.5f };
	roll_3d = 0.0f;
	yaw_3d = 0.0f;
	pitch_3d = 0.0f;

	loc_3d.y += 5.0f;

	speed = 2.0f;

	loc_2d = location;
	rot_2d = 1.0f;
	scl_2d = { FLYING_SCALE, FLYING_SCALE };
	key_list = k;

	faction = PLAYER;
	RUNTIME_FLAGS = flags;

	scr_center.x = SCREEN_WIDTH / 2;
	scr_center.y = SCREEN_HEIGHT / 2;
	ClientToScreen(hWnd, &scr_center);
	SetCursorPos(scr_center.x, scr_center.y);

	entity_list = list;
}

void Player::init_mesh(LPDIRECT3DDEVICE9 a, Model** model_list) {
	d3ddev = a;
	render_mesh = model_list[PLAYER_SHIP_MODEL];

	list = model_list;
	d3ddev = a;
}

void Player::init_sprite(LPDIRECT3DDEVICE9 a, 
			LPD3DXSPRITE b, 
			TSprite** sprite_list) {
	spritedev = b;
	tsprite = sprite_list[PLAYER_SHIP_TEX];
	rect = { 
		(LONG)loc_2d.x, 
		(LONG)loc_2d.y, 
		(LONG)(loc_2d.x + (tsprite->width * FLYING_SCALE)),
		(LONG)(loc_2d.y + (tsprite->height * FLYING_SCALE)) 
	};
}

void Player::update_camera_swing() {
	static const float cam_dist = 10.0f;
	static const float up_cam_dist = 5.0f;

	swingcam->loc = loc_3d;

	swingcam->loc.y += up_cam_dist * cos(pitch_3d);

	swingcam->loc.x += cam_dist * cos(pitch_3d) * cos(yaw_3d);
	swingcam->loc.y += cam_dist * sin(pitch_3d);
	swingcam->loc.z += cam_dist * cos(pitch_3d) * -sin(yaw_3d);
	
	swingcam->look = loc_3d;

	//swingcam->set_direction(-yaw_3d + PI, -PI * 1 / 8);
}

void Player::update_camera_fp() {
	swingcam->loc = { 0.0f, 0.0f, 0.0f };
	swingcam->loc.z += 50.0f;
	swingcam->loc.y += 7.0f;
	swingcam->set_direction(-PI / 2, 0);
}

void Player::update_physics() {
	// Here lies two weeks of insanity spent trying to get this piece
	// of code to run. Abandon hope all ye who enter here.
	D3DXVECTOR3 force_vector = { 
		thrust * cos(pitch_3d) * -cos(yaw_3d),
		thrust * -sin(pitch_3d),
		thrust * cos(pitch_3d) * sin(yaw_3d)
	};

	if (key_list[VK_SHIFT]) { velocity += force_vector; }
	if (key_list[VK_CONTROL]) { velocity -= force_vector; }
	loc_3d += velocity;
}

void Player::update_3d() {
	update_physics();
	if (key_list['A']) { yaw_3d -= turn_rate; }
	if (key_list['D']) { yaw_3d += turn_rate; }
	if (yaw_3d > PI * 2) { yaw_3d -= PI * 2; }
	if (yaw_3d < PI * 2) { yaw_3d += PI * 2; }

	if (key_list['W']) { pitch_3d += turn_rate; }
	if (key_list['S']) { pitch_3d -= turn_rate; }
	if (pitch_3d > PI * 2 + 0.5f) { pitch_3d -= PI * 2; }
	if (pitch_3d < PI * 2 - 0.5f) { pitch_3d += PI * 2; }

	if (key_list[VK_SPACE])
		createBullet();

	//if (key_list['Q']) { roll_3d -= turn_rate; }
	//if (key_list['E']) { roll_3d += turn_rate; }
	//if (roll_3d > PI * 2) { roll_3d -= PI * 2; }
	//if (roll_3d < PI * 2) { roll_3d += PI * 2; }

	// NOTE: Yaw controls are inverted (S to pitch up, W to pitch down)

	//GetCursorPos(&scr_current);

	//if (c_lock) {
	//	yaw_3d += (scr_current.x - scr_center.x) * -sensitivity * 4;

	//	pitch_3d += (scr_current.y - scr_center.y) * -sensitivity;
	//	SetCursorPos(scr_center.x, scr_center.y);
	//	ShowCursor(0);
	//}

	//if (key_list['L']) {
	//	c_lock = CURSOR_UNLOCKED;
	//	ShowCursor(1);
	//}
	//if (key_list['K']) {
	//	c_lock = CURSOR_LOCKED;
	//	SetCursorPos(scr_center.x, scr_center.y);
	//	ShowCursor(0);
	//}
}

void Player::draw_3d() {
	update_camera_swing();
	transform(d3ddev, loc_3d, D3DXVECTOR3{ roll_3d, yaw_3d, pitch_3d }, scl_3d);
	render_mesh->draw();
}

void Player::update_2d() {
	if (key_list['A'])
	{
		angle = 270.0f;
		rot_2d = angle * (PI / 180);
	}

	if (key_list['D'])
	{
		angle = 90.0f;
		rot_2d = angle * (PI / 180);
	}

	if (key_list['W']) {
		if (key_list['D'])
		{
			angle = 45.0f;
			rot_2d = angle * (PI / 180);
		}
		else if (key_list['A'])
		{
			angle = 315.0f;
			rot_2d = angle * (PI / 180);
		}
		else
		{
			angle = 0.0f;
			rot_2d = angle * (PI / 180);
		}
	}
	if (key_list['S']) {
		if (key_list['D'])
		{
			angle = 135.0f;
			rot_2d = angle * (PI / 180);
		}
		else if (key_list['A'])
		{
			angle = 225.0f;
			rot_2d = angle * (PI / 180);
		}
		else
		{
			angle = 180.0f;
			rot_2d = angle * (PI / 180);
		}
	}

	if (key_list['W'] || key_list['A'] || key_list['S'] || key_list['D']) {
		(*RUNTIME_FLAGS) |= IS_PLAYER_MOVING;
		switch ((int)angle)
		{
		case 0:		
			loc_2d.y -= speed; 
			break;
		case 45:	
			loc_2d.x += (float)sin(45 * (PI / 180))*speed; 
			loc_2d.y -= (float)cos(45 * (PI / 180))*speed; 
			break;
		case 90:	
			loc_2d.x += speed; 
			break;
		case 135:	
			loc_2d.x += (float)sin(135 * (PI / 180))*speed; 
			loc_2d.y += (float)sin(135 * (PI / 180))*speed; 
			break;
		case 180:	
			loc_2d.y += speed; 
			break;
		case 225:	
			loc_2d.x += (float)sin(225 * (PI / 180))*speed; 
			loc_2d.y -= (float)sin(225 * (PI / 180))*speed; 
			break;
		case 270:	
			loc_2d.x -= speed; 
			break;
		case 315:	
			loc_2d.x += (float)sin(315 * (PI / 180))*speed; 
			loc_2d.y += (float)sin(315 * (PI / 180))*speed; 
			break;
		}
	}
	else {
		(*RUNTIME_FLAGS) &= ~IS_PLAYER_MOVING;
	}
	//printf("x: %.2f, y: %.2f\n", loc_2d.x, loc_2d.y);
}

void Player::draw_2d() {
	tsprite->transform(loc_2d, rot_2d, scl_2d);
	tsprite->draw();
}

void Player::createBullet()
{
/*	uint8_t constructed = 0;
	for (int i = 0; i < MAX_FLYING_ENTITIES; i++)
	{
		if (!(flying_list[i]) && !constructed)
		{
			flying_list[i] = new Projectile(loc_3d, yaw_3d, pitch_3d);
			flying_list[i]->init_mesh(d3ddev, list);
			constructed = 1;
			break;
		}
	} */
		printf("Adding bullet to flying_list");
		int i = 0;
		while (entity_list[i] == NULL)
			i++;
		entity_list[i] = new Projectile(loc_3d, yaw_3d, pitch_3d);
		entity_list[i]->init_mesh(d3ddev, list);
		printf("Added bullet to flying_list");
}

void Player::display_transforms() {
	printf("roll = %.2f yaw = %.2f pitch = %.2f\n", roll_3d, yaw_3d, pitch_3d);
}
void Player::display_speed() {
	printf("\r%.2f is your speed.\n", D3DXVec3Length(&velocity));
}

void Player::Release() {
	printf("Begin Player release.\n");
	delete swingcam;
}

Player::~Player() {
	printf("End of Player destructor.\n");
}
