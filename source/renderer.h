#ifndef RENDERER_H_
#define RENDERER_H_

#include "stdafx.h"

class Renderer {
public:
	Renderer(RENDERER_INFO wad);
	~Renderer();

	void render_frame(void);

public:
	LPDIRECT3D9 d3d;    // the pointer to our Direct3D interface
	LPDIRECT3DDEVICE9 d3ddev;    // the pointer to the device class
	LPD3DXSPRITE spritedev;

private:
	void init_d3d(HWND hWnd);
	void init_light(void);
	void init_sprite(void);
	void clean_d3d(void);
	void create_resources(LPDIRECT3DDEVICE9, LPD3DXSPRITE, TSprite**, Model**);
	void destroy_resources(TSprite**, Model**);

private:
	D3DCOLOR background_color = 0x0000000;
	Light *pl1 = nullptr;
	Light *pl2 = nullptr, *pl3 = nullptr;

	Entity **ground_list;
	Entity **flying_list;

	RPrism *ground = nullptr;
	Sky *sky = nullptr;

	TSprite *map = nullptr;
	TSprite *pause = nullptr;

	uint8_t *RUNTIME_FLAGS = nullptr;

	TSprite **sprite_list = new TSprite*[MAX_SPRITES];
	Model **model_list = new Model*[MAX_MODELS];

	LPD3DXFONT def_font;
};

#endif // RENDERER_H_
