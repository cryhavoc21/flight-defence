// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#ifndef STDAFX_H_
#define STDAFX_H_

#include "targetver.h"
#include "../resource.h"

#include <stdio.h>
#include <tchar.h>
#include <string>
#include <cmath>
#include <cstdint>
#include <cstdlib>

#include <crtdbg.h>		// Allows for _Crt memory leak debugging
#include <afxwin.h>		// Allows for MFC and multithreading

// Allows for DirectX to be used
#include <d3d9.h>
#include <d3dx9.h> 
#include <D3D9Types.h>
#include <D3DX9Mesh.h>
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

// This definition is necessary to track memory leaks with _Crt
#define _CRTDBG_MAP_ALLOC

// These definitions are for the screen size, which is currently fixed
// at compile-time. A runtime solution must be found before release.
#define SCREEN_SCALE 1.0
#define SCREEN_WIDTH ((UINT)(800 * SCREEN_SCALE))
#define SCREEN_HEIGHT ((UINT)(600 * SCREEN_SCALE))
#define SCREEN_X 0
#define SCREEN_Y 0

// PI constant necessary for compilation of camera.h and others
#define PI 3.141592654f

#define UPDATE_MILLIS 16	// Number of milliseconds per frame
#define FLYING_SCALE 0.25f	// The fixed scale for each of the flying entities.
#define GROUND_SCALE 1.0f	// The fixed scale for each of the ground entities.
#define MAX_FLYING_ENTITIES 1024	// Max number of flying entities
#define MAX_GROUND_ENTITIES 1024	// Max number of ground entities
#define MAX_SPRITES 1024	// Max number of sprite resources
#define MAX_MODELS 1024		// Max number of model resources

// Enum that store the faction of each entity
typedef enum FACTION_TYPE { NONE, PLAYER, REDS } FACTION_TYPE;

// Enum that stores the types of bases (I still think we need to make 
// individual classes. -Daniel)
typedef enum BASE_TYPE { MAIN, CAPTURE } BASE_TYPE;

// Flags for the RUNTIME_FLAGS object
#define IS_UPDATE_THREAD 1			// 0000 0001	Is the updating thread alive? 
//	Flip this to tell the thread to kill itself
#define DO_2D 2				// 0000 0010	Is the program running in 2D?
#define IS_UPDATING 4		// 0000 0100	Is the entity list running update()?
#define IS_UPDATER_DEAD 8	// 0000 1000	Is the updating thread done killing itself?
#define IS_PLAYER_MOVING 16	// 0001 0000	Is the Player object moving?
#define IS_PAUSED 32		// 0010 0000	Is the game paused?

typedef enum MODEL_TYPE { 
	PLAYER_SHIP_MODEL, BULLET_MODEL 
} MODEL_TYPE;
typedef enum TEXTURE_TYPE { 
	PLAYER_SHIP_TEX, 
	ENEMY_SHIP_TEX, 
	PLAYER_AIRFIELD_TEX, 
	AIRFIELD_TEX, 
	MAP_TEX,
	AA_GUN_TEX,
	ENGAGEMENT_TEX,
	PAUSE_TEX
} TEXTURE_TYPE;

typedef enum USE_GRAVITY { GRAV_DISABLED, GRAV_ENABLED } USE_GRAVITY;

// These includes must be run in a very specific order, since 
// certain classes have certain dependencies that aren't immediately
// apparent. If you encounter some kind of "No type identifier, int assumed"
// error, check these includes to see if it's correct. Known issues are 
// documented as follows:
#include "light.h"
#include "camera.h"

// vertex.h must be defined before r_prism.h
#include "util/util_vertex.h"
#include "r_prism.h"
#include "sky.h"

// model.h and tsprite.h must be defined before entity.h
#include "model.h"
#include "tsprite.h"
#include "entity.h"

// physbody.h must be defined before player.h, and player.h
// must be defined after entity.h
#include "physbody.h"
#include "player.h"
#include "enemy.h"
#include "proj.h" //because this needs to go somewhere

// These struct definitions must come after the definition
// of any of the members, but before renderer.h

// struct of the pointers passed to the Renderer object
struct RENDERER_INFO {
	HWND *win_ptr;
	Entity **flying_list;
	Entity **ground_list;
	uint8_t *RUNTIME_FLAGS;
};

// struct of pointers passed to the Update thread. Will be a void*
// cast as this struct.
struct UPDATE_INFO {
	Entity **flying_list;
	Entity **ground_list;
	uint8_t *RUNTIME_FLAGS;
}; 

#include "util/util_rend.h"
#include "util/util_vect.h"

// For safety's sake, just put renderer.h at the end. Nothing should be dependent
// on it, and it should depenend on everything beforehand.
#include "renderer.h"

#endif // STDAFX_H_
