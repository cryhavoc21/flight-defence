#ifndef PHYSBODY_H_
#define PHYSBODY_H_

#define FORCE_SCALE (1.0f / 6000.0f)
#define MAX_FORCES 20

class PhysBody {
public:
	PhysBody() {}
	PhysBody(USE_GRAVITY is_gravity);
	~PhysBody() {}

	void add_force(D3DXVECTOR3 force);
	void edit_force(D3DXVECTOR3 force, int force_index);

	D3DXVECTOR3 get_resultant();

	void Release();

private:
	D3DXVECTOR3 gravity{ 0.0f, -9.8f, 0.0f };
	D3DXVECTOR3 *force_list[MAX_FORCES];
	
	int is_grav = 0;
	int num_forces = 0;
	float i, j, k;
};


#endif // PHYSBODY_H_