
#include "stdafx.h"
#include "renderer.h"

Renderer::Renderer(RENDERER_INFO wad) {
	init_d3d(*(wad.win_ptr));
	init_light();
	init_sprite();
	pl1 = new Light(d3ddev, -1.0f, -0.3f, -1.0f, 0, D3DLIGHT_DIRECTIONAL);

	ground_list = wad.ground_list;
	flying_list = wad.flying_list;
	RUNTIME_FLAGS = wad.RUNTIME_FLAGS;

	create_resources(d3ddev, spritedev, sprite_list, model_list);

	for (int i = 0; i < MAX_GROUND_ENTITIES; i++) {
		if (ground_list[i]) {
			ground_list[i]->init_mesh(d3ddev, model_list);
			ground_list[i]->init_sprite(d3ddev, spritedev, sprite_list);
		}
	}
	for (int i = 0; i < MAX_FLYING_ENTITIES; i++) {
		if (flying_list[i]) {
			flying_list[i]->init_mesh(d3ddev, model_list);
			flying_list[i]->init_sprite(d3ddev, spritedev, sprite_list);
		}
	}

	if (FAILED(load_font(d3ddev, &def_font, L"Arial"))) {
		printf("Failed to load font\n");
	}
	
	ground = new RPrism(d3ddev);
	sky = new Sky(d3ddev);
	map = sprite_list[MAP_TEX];
	pause = sprite_list[PAUSE_TEX];
}

void Renderer::create_resources(LPDIRECT3DDEVICE9 d3ddev, 
		LPD3DXSPRITE spritedev, 
		TSprite** sprites, 
		Model** models) {

	ZeroMemory(sprites, sizeof(TSprite*) * MAX_SPRITES);
	ZeroMemory(models, sizeof(Model*) * MAX_MODELS);

	sprites[PLAYER_SHIP_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//newPlayerShip.png", 
		128, 128);
	sprites[ENEMY_SHIP_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//enemyShip.png", 
		128, 128);
	sprites[PLAYER_AIRFIELD_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//playerAirfield.png", 
		128, 128);
	sprites[AIRFIELD_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//airfield.png", 
		128, 128);
	sprites[MAP_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//backgroundDefault.png", 
		800, 600);
	sprites[AA_GUN_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//aaGun.png", 
		16, 16);
	sprites[ENGAGEMENT_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//engagementArea.png", 
		256, 256);
	sprites[PAUSE_TEX] = new TSprite(d3ddev, 
		spritedev, 
		L"media//pause.png", 
		800, 600);

	models[PLAYER_SHIP_MODEL] = new Model(d3ddev, L"media//shuttle.x");
	models[BULLET_MODEL] = new Model(d3ddev, L"media//bullet.x");
}

void Renderer::destroy_resources(TSprite** sprites, Model** models) {
	for (int i = 0; i < MAX_SPRITES; i++) {
		if (sprites[i]) {
			sprites[i]->Release();
			delete sprites[i];
		}
	}
	delete sprites;

	for (int i = 0; i < MAX_MODELS; i++) {
		if (models[i]) {
			models[i]->Release();
			delete models[i];
		}
	}
	delete models;
}

Renderer::~Renderer() {
	delete pl1;

	ground->Release();
	delete ground;
	sky->Release();
	delete sky;

	clean_d3d();

	def_font->Release();

	destroy_resources(sprite_list, model_list);
}

void Renderer::render_frame(void) {
	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, background_color, 1.0f, 0);
	d3ddev->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
	d3ddev->BeginScene();
	
	if (*(RUNTIME_FLAGS) & DO_2D) {
		spritedev->Begin(D3DXSPRITE_ALPHABLEND);

		map->transform(D3DXVECTOR2{ 0.0f, 0.0f }, 
			0.0f, 
			D3DXVECTOR2{ 0.75f, 0.55f });
		map->draw();

		RECT area = { 50, 50, 500, 100 };
		if (FAILED(def_font->DrawTextW(spritedev,
			L"HELLO WORLD!",
			-1, // Null terminated string
			&area,
			DT_TOP | DT_LEFT,
			0xffffffff))) {
			printf("Failed to print string\n");
		}

		//printf("Begin player drawing 2d\n");

		for (int i = 0; i < MAX_GROUND_ENTITIES; i++) {
			if (ground_list[i]) {
				ground_list[i]->draw_2d();
			}
		}
		for (int i = 0; i < MAX_FLYING_ENTITIES; i++) {
			if (flying_list[i]) {
				flying_list[i]->draw_2d();
			}
		}

		spritedev->End();
	}
	else {
#pragma warning(suppress: 6011)
		position_camera(d3ddev, (Camera*)(((Player*)flying_list[0])->swingcam));
		d3ddev->SetRenderState(D3DRS_LIGHTING, FALSE);
		ground->transform(D3DXVECTOR3{ 0.0f, 0.0f, 0.0f },
			D3DXVECTOR3{ 0.0f, 0.0f, 0.0f },
			D3DXVECTOR3{ 500.0f, 0.05f, 500.0f });
		ground->draw();
		transform(d3ddev, D3DXVECTOR3{ 0.0f, 0.0f, 0.0f },
			D3DXVECTOR3{ 0.0f, 0.0f, 0.0f },
			D3DXVECTOR3{ 600.0f, 600.0f, 600.0f });
		sky->draw();
		d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);
		
		//printf("Being player drawing 3d\n");
		for (int i = 0; i < MAX_GROUND_ENTITIES; i++) {
			if (ground_list[i]) {
				ground_list[i]->draw_3d();
			}
		}
		for (int i = 0; i < MAX_FLYING_ENTITIES; i++) {
			if (flying_list && flying_list[i]) {
				flying_list[i]->draw_3d();
			}
		}
	}

	if (*(RUNTIME_FLAGS) & IS_PAUSED) {
		spritedev->Begin(D3DXSPRITE_ALPHABLEND);

		pause->transform(D3DXVECTOR2{ 0.0f, 0.0f }, 
			0.0f, 
			D3DXVECTOR2{ 0.75f, 0.55f });
		pause->draw();

		spritedev->End();
	}
		
	d3ddev->EndScene();
	d3ddev->Present(NULL, NULL, NULL, NULL);
}

void Renderer::init_d3d(HWND hWnd) {
	d3d = Direct3DCreate9(D3D_SDK_VERSION);    // create the Direct3D interface

	D3DPRESENT_PARAMETERS device_info;

	ZeroMemory(&device_info, sizeof(device_info));
	device_info.Windowed = TRUE;    // program windowed, not fullscreen
	device_info.SwapEffect = D3DSWAPEFFECT_DISCARD;    // discard old frames
	device_info.hDeviceWindow = hWnd;
	device_info.BackBufferFormat = D3DFMT_X8R8G8B8;
	device_info.BackBufferWidth = SCREEN_WIDTH;
	device_info.BackBufferHeight = SCREEN_HEIGHT;
	device_info.EnableAutoDepthStencil = TRUE;
	device_info.AutoDepthStencilFormat = D3DFMT_D16;
	// Disable VSync
	device_info.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	// create a device class using this information and the info from the device_info stuct
	d3d->CreateDevice(D3DADAPTER_DEFAULT,
			D3DDEVTYPE_HAL,
			hWnd,
			D3DCREATE_SOFTWARE_VERTEXPROCESSING,
			&device_info,
			&d3ddev);
	
	int ambient = 0x7f7f7f;	// 127, 127, 127 (half-light)
	d3ddev->SetRenderState(D3DRS_LIGHTING, TRUE);    // turn on the 3D lighting
	d3ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	d3ddev->SetRenderState(D3DRS_ZENABLE, TRUE);	// enable zbuffering
	d3ddev->SetRenderState(D3DRS_AMBIENT, ambient); // set ambient light to half-lit
	d3ddev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);	// enable normals
	d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);	// enable alpha (transparency)
	d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);	// enable blending
	d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	// set source blending
	d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// set destination blending
}

void Renderer::init_light(void) {
	D3DMATERIAL9 material;

	ZeroMemory(&material, sizeof(D3DMATERIAL9));    // clear out the struct for use
	material.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set diffuse color to white
	material.Ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set ambient color to white

	d3ddev->SetMaterial(&material);    // set the globably-used material to &material
}

void Renderer::init_sprite(void) {
	if(FAILED(D3DXCreateSprite(d3ddev, &spritedev)))
		printf("Error creating sprite");
}

void Renderer::clean_d3d(void) {
	spritedev->Release();
	d3ddev->Release();
	d3d->Release();
}
