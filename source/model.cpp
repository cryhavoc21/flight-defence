#include "stdafx.h"

#include "model.h"

Model::Model(LPDIRECT3DDEVICE9 a, LPCWSTR path) {
	d3ddev = a;
	init_geometry(path);
}

void Model::init_geometry(LPCWSTR path) {
	LPD3DXBUFFER materialBuffer;						// temporary material buffer
	
	// load in model from .x file
	if (FAILED(D3DXLoadMeshFromX(path, D3DXMESH_SYSTEMMEM,
			d3ddev, NULL,
			&materialBuffer, NULL, &numMaterials,
			&mesh))) {
		printf("Failed to load model\n");
	//			If model is not in current folder, try parent folder
		// If texture is not in current folder, try parent folder
		const CHAR* strPrefix = "..\\";
		CHAR strModel[MAX_PATH];
		strcpy_s(strModel, MAX_PATH, strPrefix);
		strcat_s(strModel, MAX_PATH, (char*)path);
		if (FAILED(D3DXLoadMeshFromX((LPCWSTR)strModel, D3DXMESH_SYSTEMMEM,
				d3ddev, NULL,
				&materialBuffer, NULL, &numMaterials,
				&mesh))) {
			printf("failed to find\n");
		}
	}

	// create an array of materials
	D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)materialBuffer->GetBufferPointer();
	meshMaterials = new D3DMATERIAL9[numMaterials];
	if (!meshMaterials) {
		printf("materials out of memory\n");
	}
	meshTextures = new LPDIRECT3DTEXTURE9[numMaterials];
	if (!meshTextures) {
		printf("textures out of memory\n");
	}

	for (DWORD i = 0; i < numMaterials; i++) {
		meshMaterials[i] = d3dxMaterials[i].MatD3D;
		meshMaterials[i].Ambient = meshMaterials[i].Diffuse;
		meshTextures[i] = 0;

		if (d3dxMaterials[i].pTextureFilename != NULL &&
				lstrlenA(d3dxMaterials[i].pTextureFilename) > 0) {
			if (FAILED(D3DXCreateTextureFromFileA(d3ddev,
					d3dxMaterials[i].pTextureFilename,
					&meshTextures[i]))) {
				// If texture is not in current folder, try parent folder
				const CHAR* strPrefix = "..\\";
				CHAR strTexture[MAX_PATH];
				strcpy_s(strTexture, MAX_PATH, strPrefix);
				strcat_s(strTexture, MAX_PATH, d3dxMaterials[i].pTextureFilename);
				// If texture is not in current folder, try parent folder
				if (FAILED(D3DXCreateTextureFromFileA(d3ddev,
						strTexture,
						&meshTextures[i]))) {
					printf("cannot find texture file\n");
				}
			}
		}
	}

	DWORD fvf = mesh->GetFVF();
	if (!(fvf & D3DFVF_NORMAL)) {			// If the mesh does not contain normals
		fvf |= D3DFVF_NORMAL;				// "or" the FVF to contain normals
		ID3DXMesh *newMesh;					// temporary new mesh

		// clone in new mesh
		if (FAILED(mesh->CloneMeshFVF(0, fvf, d3ddev, &newMesh))) {
			printf("failed to clone mesh\n");
		}
		mesh->Release();					// release old mesh
		mesh = newMesh;						// replace with clone
	}
	D3DXComputeNormals(mesh, NULL);			// compute normals for the mesh

	materialBuffer->Release();				// release temporary buffer for materials
}

void Model::draw() {
	for (DWORD i = 0; i < numMaterials; i++) {
		// Set the material and texture for this subset
		d3ddev->SetMaterial(&meshMaterials[i]);
		d3ddev->SetTexture(0, meshTextures[i]);

		// Draw the mesh subset
		mesh->DrawSubset(i);
	}
}

void Model::Release() {
	printf("Release model\n");
	if (meshMaterials)
		delete[] meshMaterials;
	for (DWORD i = 0; i < numMaterials; i++){
		if (meshTextures[i]) {
			meshTextures[i]->Release();
		}
	}
	if (meshTextures)
		delete[] meshTextures;
	mesh->Release();
}
