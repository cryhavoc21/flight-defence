#include "stdafx.h"
#include "proj.h"

Projectile::Projectile()
{
	loc_3d = D3DXVECTOR3{ 0.0F, 0.0F, 0.0F };
	yaw = 0;
	pitch = 0;
	velocity = 1;
}

Projectile::Projectile(D3DXVECTOR3 playerLocation, float fireYawAngle, float firePitchAngle)
{
	loc_3d = playerLocation;
	yaw = fireYawAngle;
	pitch = firePitchAngle;
	velocity = 10.0F;
}

Projectile::~Projectile()
{
	Release();
}

void Projectile::init_mesh(LPDIRECT3DDEVICE9 a, Model** model_list)
{
	d3ddev = a;
	render_mesh = model_list[BULLET_MODEL];
}

void Projectile::update_3d()
{
	D3DXVECTOR3 vector1 = {
		velocity * cos(pitch_3d) * -cos(yaw_3d),
		velocity * -sin(pitch_3d),
		velocity * cos(pitch_3d) * sin(yaw_3d)
	};

	loc_3d += vector1;
}

void Projectile::draw_3d()
{
	transform(d3ddev, loc_3d, D3DXVECTOR3{ 0, yaw_3d, pitch_3d }, scl_3d);
	render_mesh->draw();
}

void Projectile::Release()
{

}