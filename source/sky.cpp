#include "stdafx.h"
#include "sky.h"

Sky::Sky(LPDIRECT3DDEVICE9 d) {
	d3ddev = d;
#define cube_len 1.0f
	DWORD sky_color = D3DCOLOR_ARGB(255, 135, 206, 250);
	DWORD sky_top = 0xffeeeeee;
	static const uint8_t num_vertices = 24;
	CUSTOM_COLOR_VERTEX vertices[] = {
		{ -cube_len, -cube_len, cube_len, 0.0f, 0.0f, -1.0f, sky_color },    // side 1
		{ cube_len, -cube_len, cube_len, 0.0f, 0.0f, -1.0f, sky_color },
		{ -cube_len, cube_len, cube_len, 0.0f, 0.0f, -1.0f, sky_color | sky_top },
		{ cube_len, cube_len, cube_len, 0.0f, 0.0f, -1.0f, sky_color | sky_top },

		{ -cube_len, -cube_len, -cube_len, 0.0f, 0.0f, 1.0f, sky_color  },    // side 2
		{ -cube_len, cube_len, -cube_len, 0.0f, 0.0f, 1.0f, sky_color | sky_top },
		{ cube_len, -cube_len, -cube_len, 0.0f, 0.0f, 1.0f, sky_color },
		{ cube_len, cube_len, -cube_len, 0.0f, 0.0f, 1.0f, sky_color | sky_top },

		{ -cube_len, cube_len, -cube_len, 0.0f, -1.0f, 0.0f, sky_color | sky_top },    // Top
		{ -cube_len, cube_len, cube_len, 0.0f, -1.0f, 0.0f, sky_color | sky_top },
		{ cube_len, cube_len, -cube_len, 0.0f, -1.0f, 0.0f, sky_color | sky_top },
		{ cube_len, cube_len, cube_len, 0.0f, -1.0f, 0.0f, sky_color | sky_top },

		{ -cube_len, -cube_len, -cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 0) },    // Bottom
		{ cube_len, -cube_len, -cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 0) },
		{ -cube_len, -cube_len, cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 0) },
		{ cube_len, -cube_len, cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 0) },

		{ cube_len, -cube_len, -cube_len, -1.0f, 0.0f, 0.0f, sky_color  },    // side 5
		{ cube_len, cube_len, -cube_len, -1.0f, 0.0f, 0.0f, sky_color | sky_top },
		{ cube_len, -cube_len, cube_len, -1.0f, 0.0f, 0.0f, sky_color },
		{ cube_len, cube_len, cube_len, -1.0f, 0.0f, 0.0f, sky_color | sky_top },

		{ -cube_len, -cube_len, -cube_len, 1.0f, 0.0f, 0.0f, sky_color  },    // side 6
		{ -cube_len, -cube_len, cube_len, 1.0f, 0.0f, 0.0f, sky_color  },
		{ -cube_len, cube_len, -cube_len, 1.0f, 0.0f, 0.0f, sky_color | sky_top },
		{ -cube_len, cube_len, cube_len, 1.0f, 0.0f, 0.0f, sky_color | sky_top },
	};

	short indices[] = {
		2, 1, 0,
		3, 1, 2,
		6, 5, 4,
		7, 5, 6,
		10, 9, 8,
		11, 9, 10,
		14, 13, 12,
		15, 13, 14,
		18, 17, 16,
		19, 17, 18,
		22, 21, 20,
		23, 21, 22
	};

	if (FAILED(d3ddev->CreateVertexBuffer(num_vertices * sizeof(CUSTOM_COLOR_VERTEX),
		D3DUSAGE_WRITEONLY,
		CUSTOM_COLOR_FVF,
		D3DPOOL_MANAGED,
		&vert_buffer,
		NULL))) printf("Vertex Buffered Failed\n");

	if (FAILED(d3ddev->CreateIndexBuffer(sizeof(indices),
		0,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&index_buffer,
		NULL))) printf("Index Buffer Failed\n");

	VOID* pVoid;

	vert_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	vert_buffer->Unlock();

	index_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, indices, sizeof(indices));
	index_buffer->Unlock();
}

void Sky::Release() {
	//vert_buffer->Release();
	//index_buffer->Release();
}

Sky::~Sky() {

}

void Sky::draw() {
	d3ddev->SetFVF(CUSTOM_COLOR_FVF);

	d3ddev->SetStreamSource(0, vert_buffer, 0, sizeof(CUSTOM_COLOR_VERTEX));
	d3ddev->SetIndices(index_buffer);
	d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 24, 0, 12);
}

void Sky::transform(D3DXVECTOR3 loc, D3DXVECTOR3 rot, D3DXVECTOR3 scl) {
	// a matrix to store the rotation information
	D3DXMATRIX matRotateX, matRotateY, matRotateZ, matTrans, matScale;

	D3DXMatrixRotationX(&matRotateX, rot.x);
	D3DXMatrixRotationY(&matRotateY, rot.y);
	D3DXMatrixRotationZ(&matRotateZ, rot.z);
	D3DXMatrixTranslation(&matTrans, loc.x, loc.y, loc.z);
	D3DXMatrixScaling(&matScale, scl.x, scl.y, scl.z);

	d3ddev->SetTransform(D3DTS_WORLD, &(matRotateY * matRotateX * matRotateZ * matTrans * matScale));
}
