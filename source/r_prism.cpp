#include "stdafx.h"
#include "r_prism.h"

RPrism::RPrism(LPDIRECT3DDEVICE9 a) {
	d3ddev = a;

	CUSTOM_COLOR_VERTEX vertices[] = {
		{ -cube_len, -cube_len, cube_len, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },    // side 1
		{ cube_len, -cube_len, cube_len, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },
		{ -cube_len, cube_len, cube_len, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },
		{ cube_len, cube_len, cube_len, 0.0f, 0.0f, 1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },

		{ -cube_len, -cube_len, -cube_len, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },    // side 2
		{ -cube_len, cube_len, -cube_len, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },
		{ cube_len, -cube_len, -cube_len, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },
		{ cube_len, cube_len, -cube_len, 0.0f, 0.0f, -1.0f, D3DCOLOR_ARGB(255, 255, 0, 0) },

		{ -cube_len, cube_len, -cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },    // side 3
		{ -cube_len, cube_len, cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },
		{ cube_len, cube_len, -cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },
		{ cube_len, cube_len, cube_len, 0.0f, 1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },

		{ -cube_len, -cube_len, -cube_len, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },    // side 4
		{ cube_len, -cube_len, -cube_len, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },
		{ -cube_len, -cube_len, cube_len, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },
		{ cube_len, -cube_len, cube_len, 0.0f, -1.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 255, 0) },

		{ cube_len, -cube_len, -cube_len, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },    // side 5
		{ cube_len, cube_len, -cube_len, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },
		{ cube_len, -cube_len, cube_len, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },
		{ cube_len, cube_len, cube_len, 1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },

		{ -cube_len, -cube_len, -cube_len, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },    // side 6
		{ -cube_len, -cube_len, cube_len, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },
		{ -cube_len, cube_len, -cube_len, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },
		{ -cube_len, cube_len, cube_len, -1.0f, 0.0f, 0.0f, D3DCOLOR_ARGB(255, 0, 0, 255) },
	};

	short indices[] = {
		0, 1, 2,    // side 1
		2, 1, 3,
		4, 5, 6,    // side 2
		6, 5, 7,
		8, 9, 10,    // side 3
		10, 9, 11,
		12, 13, 14,    // side 4
		14, 13, 15,
		16, 17, 18,    // side 5
		18, 17, 19,
		20, 21, 22,    // side 6
		22, 21, 23,
	};

	if (FAILED(d3ddev->CreateVertexBuffer(num_vertices * sizeof(CUSTOM_COLOR_VERTEX),
				D3DUSAGE_WRITEONLY,
				CUSTOM_COLOR_FVF,
				D3DPOOL_MANAGED,
				&vert_buffer,
				NULL))) printf("Vertex Buffered Failed\n");

	if (FAILED(d3ddev->CreateIndexBuffer(sizeof(indices),
				0,
				D3DFMT_INDEX16,
				D3DPOOL_MANAGED,
				&index_buffer,
				NULL))) printf("Index Buffer Failed\n");

	VOID* pVoid;

	vert_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	vert_buffer->Unlock();

	index_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, indices, sizeof(indices));
	index_buffer->Unlock();
}

void RPrism::Release() {
	vert_buffer->Release();
	index_buffer->Release();
}

RPrism::~RPrism() {
	
}

void RPrism::transform(D3DXVECTOR3 loc, D3DXVECTOR3 rot, D3DXVECTOR3 scl) {
	// a matrix to store the rotation information
	D3DXMATRIX matRotateX, matRotateY, matRotateZ, matTrans, matScale;

	D3DXMatrixRotationX(&matRotateX, rot.x);
	D3DXMatrixRotationY(&matRotateY, rot.y);
	D3DXMatrixRotationZ(&matRotateZ, rot.z);
	D3DXMatrixTranslation(&matTrans, loc.x, loc.y, loc.z);
	D3DXMatrixScaling(&matScale, scl.x, scl.y, scl.z);

	d3ddev->SetTransform(D3DTS_WORLD, &(matRotateY * matRotateX * matRotateZ * matTrans * matScale));
}

void RPrism::draw() {
	d3ddev->SetFVF(CUSTOM_COLOR_FVF);

	d3ddev->SetStreamSource(0, vert_buffer, 0, sizeof(CUSTOM_COLOR_VERTEX));
	d3ddev->SetIndices(index_buffer);
	d3ddev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertices, 0, 12);
}