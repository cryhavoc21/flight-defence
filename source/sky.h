#include "stdafx.h"

#ifndef SKY_H_
#define SKY_H_

class Sky {
public:
	Sky() { printf("wrong constructor, idiot\n"); }
	Sky(LPDIRECT3DDEVICE9);
	~Sky();
	void Release();

	virtual void draw();
	void transform(D3DXVECTOR3 loc, D3DXVECTOR3 rot, D3DXVECTOR3 scl);
private:
	LPDIRECT3DVERTEXBUFFER9 vert_buffer;
	LPDIRECT3DINDEXBUFFER9 index_buffer;

	LPDIRECT3DDEVICE9 d3ddev;
};

#endif //SKY_H_
