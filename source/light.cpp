#include "stdafx.h"

#include "light.h"

// TODO: FIX THIS PIECE OF CRAP OF A CLASS YOU IDIOT, DANIEL!
// BEST WISHES, 
// DANIEL

Light::Light(LPDIRECT3DDEVICE9 d3ddev, float x, float y, float z, int a, int light_type) {
	light_num = a;

	D3DLIGHT9 light;
	ZeroMemory(&light, sizeof(light));    // clear out the light struct for use
	light.Type = (D3DLIGHTTYPE) light_type;    // make the light type 'point light'
	light.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);    // set the light's color
	light.Direction = D3DXVECTOR3(x, y, z);
	light.Range = 100.0f;    // a range of 100
	light.Attenuation0 = 0.0f;    // no constant inverse attenuation
	light.Attenuation1 = 0.050f;    // only .125 inverse attenuation
	light.Attenuation2 = 0.0f;    // no square inverse attenuation

	d3ddev->SetLight(a, &light);    // send the light struct properties to light #a
	d3ddev->LightEnable(a, TRUE);    // turn on light #a
}
