#include "stdafx.h"
#include "physbody.h"

PhysBody::PhysBody(USE_GRAVITY is_gravity) {
	ZeroMemory(force_list, sizeof(D3DXVECTOR3*) * MAX_FORCES);

	is_grav = is_gravity;
	if (is_grav == GRAV_ENABLED) {
		*(force_list + num_forces++) = &gravity;
	}
}

void PhysBody::add_force(D3DXVECTOR3 force) {
	*(force_list + num_forces++) = &force;
}

void PhysBody::edit_force(D3DXVECTOR3 force, int num) {
	*(force_list + num) = &force;
}

D3DXVECTOR3 PhysBody::get_resultant() {
	int c;
	i = 0.0f, j = 0.0f , k = 0.0f;
	for (c = 0; c < num_forces; c++) {
		i += force_list[c]->x;
		j += force_list[c]->y;
		k += force_list[c]->z;
	}

	return D3DXVECTOR3{ i * FORCE_SCALE, j * FORCE_SCALE, k * FORCE_SCALE };
}

void PhysBody::Release() {
	for (int i = 0; i < num_forces; i++) {
		if (force_list[i]) {
			delete force_list[i];
		}
	}
}
