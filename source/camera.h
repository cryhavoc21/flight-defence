#include "stdafx.h"

#ifndef CAMERA_H_
#define CAMERA_H_

#define UPPER ((PI / 2) - 0.0001f)
#define LOWER (-(PI / 2) + 0.0001f)

class Camera {
public:
	Camera();
	Camera(float a, float b, float c);
	~Camera();
	void set_direction(float xz, float yz);

	D3DXVECTOR3 loc, look, up;
protected:
	enum CURSOR_LOCK {CURSOR_UNLOCKED, CURSOR_LOCKED};
	int c_lock = CURSOR_LOCKED;
	float sensitivity = 0.003f;

	float dir_xz;
	float dir_yz;
};

#endif // CAMERA_H_