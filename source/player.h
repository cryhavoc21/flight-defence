#include "stdafx.h"

#ifndef PLAYER_H_
#define PLAYER_H_

class Player : public Entity{
public:
	Player() {}
	Player(D3DXVECTOR2 location, int *key_list, uint8_t*, HWND, Entity** list);
	~Player();
	
	void init_mesh(LPDIRECT3DDEVICE9 d3ddev, Model**);
	void init_sprite(LPDIRECT3DDEVICE9 a, LPD3DXSPRITE spritedev, TSprite**);
	void Release();
	void draw_3d();
	void draw_2d();
	void update_3d();
	void update_2d();

	void createBullet();

	void display_transforms();
	void display_speed();

	Camera *swingcam = 0;

private:
	D3DXVECTOR3 velocity{ 0.0f, 0.0f, 0.0f };
	float angle = 0; //in degrees
	float speed;
	bool key_down;
	
	void update_camera_swing();
	void update_camera_fp();
	void update_physics();

	int *key_list;
	enum CURSOR_LOCK { CURSOR_UNLOCKED, CURSOR_LOCKED };
	int c_lock = CURSOR_LOCKED;

	HWND hWnd;
	POINT scr_center;
	POINT scr_current;

	float throttle = 1.0f;
	float turn_rate = 0.01f;
	float thrust = 0.01f;
	float sensitivity = 0.0009f;

	Model** list;
	Entity** entity_list;
	LPDIRECT3DDEVICE9 d3ddev;
};

#endif //PLAYER_H_
