#include "stdafx.h"

#ifndef MODEL_H_
#define MODEL_H_

class Model {
public:
	Model() {}
	Model(LPDIRECT3DDEVICE9 a, LPCWSTR path);
	~Model() {}

	void draw();
	void Release();

private:
	LPDIRECT3DDEVICE9 d3ddev;
	LPD3DXMESH mesh = 0;
	D3DMATERIAL9 *meshMaterials = 0;
	LPDIRECT3DTEXTURE9 *meshTextures = 0;
	DWORD numMaterials = 0L;

private:
	void init_geometry(LPCWSTR path);
};

#endif //MODEL_H_