#include "stdafx.h"

#ifndef R_PRISM_H_
#define R_PRISM_H_

class RPrism {
public:
	RPrism() {}
	RPrism(LPDIRECT3DDEVICE9 a);
	virtual ~RPrism();
	void Release();

	virtual void draw();
	void transform(D3DXVECTOR3 loc, D3DXVECTOR3 rot, D3DXVECTOR3 scl);
private:
	LPDIRECT3DVERTEXBUFFER9 vert_buffer;
	LPDIRECT3DINDEXBUFFER9 index_buffer;

	LPDIRECT3DDEVICE9 d3ddev;

	const float cube_len = 1.0f;
	const int num_vertices = 24;
};

#endif // R_PRISM_H_