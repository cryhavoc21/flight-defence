#include "stdafx.h"

#ifndef LIGHT_H_
#define LIGHT_H_

class Light {
public:
	Light() {}
	Light(LPDIRECT3DDEVICE9 d3ddev, float x, float y, float z, int light_num, int light_type);
	~Light() {}

	int get_light_num() { return light_num; }

private:
	int light_num;
};

#endif // LIGHT_H_