#include "stdafx.h"

#ifndef SKYBOX_H_
#define SKYBOX_H_

class Skybox {
public:
	Skybox() {}
	Skybox(LPDIRECT3DDEVICE9);
	~Skybox();
	void Release();
	void draw();
public:
	LPDIRECT3DDEVICE9 d3ddev;
	LPDIRECT3DVERTEXBUFFER9 vert_buffer;
	LPDIRECT3DINDEXBUFFER9 index_buffer;

	LPDIRECT3DTEXTURE9 texture = 0;
	const static int max_x = 10;
	const static int max_y = 10;
	const static int max_z = 10;
};

#endif //SKYBOX_H_
