#include "stdafx.h"
#include "camera.h"

Camera::Camera() {
	loc = { 0.0f, 0.0f, 0.0f };
	look = { 0.0f, 0.0f, 0.0f };
	up = { 0.0f, 1.0f, 0.0f };

	dir_xz = 0.0f;
	dir_yz = 0.0f;
}

Camera::Camera(float x, float y, float z) {
	loc.x = look.x = x;
	loc.y = look.y = y;
	loc.z = look.z = z;
	
	up = { 0.0f, 1.0f, 0.0f };

	//dir_xz = -(PI / 2.0f);
	dir_xz = 0.0f;
	dir_yz = 0.0f;
}

void Camera::set_direction(float xz, float yz) {
	dir_xz = xz;
	dir_yz = yz;
	look = loc;
	look.x += cos(dir_xz) * cos(dir_yz);
	look.y += sin(dir_yz);
	look.z += sin(dir_xz) * cos(dir_yz);

	//print_vector_3d(look);
}

Camera::~Camera() {

}
