#include "stdafx.h"
#include "enemy.h"

Enemy::Enemy(D3DXVECTOR2 location, Entity** list1, Entity** list2, uint8_t* flags) {
	phys_body->add_force(thrust);
	phys_body->add_force(lift);

	loc_3d.y += 5.0f;

	loc_2d = location;
	rot_2d = 1.0f;
	scl_2d = { FLYING_SCALE, FLYING_SCALE };

	thrust.z = -1.0f;
	lift.y = 100.0f;
	phys_body->edit_force(thrust, 1);
	phys_body->edit_force(lift, 2);

	flying_list = list1;
	ground_list = list2;

	faction = REDS;
	RUNTIME_FLAGS = flags;
}

void Enemy::update_physics() {
	D3DXVECTOR3 resultant = phys_body->get_resultant();

	velocity += resultant;
	loc_3d += velocity;
}

void Enemy::init_mesh(LPDIRECT3DDEVICE9 a, Model** model_list) {
	d3ddev = a;
	render_mesh = model_list[PLAYER_SHIP_MODEL];
}

void Enemy::init_sprite(LPDIRECT3DDEVICE9 a, LPD3DXSPRITE b, TSprite** sprite_list) {
	spritedev = b;
	tsprite = sprite_list[ENEMY_SHIP_TEX];
	engage_circle = sprite_list[ENGAGEMENT_TEX];

	rect = { (LONG)loc_2d.x, 
		(LONG)loc_2d.y, 
		(LONG)(loc_2d.x + (tsprite->width * FLYING_SCALE)),
		(LONG)(loc_2d.y + (tsprite->height  * FLYING_SCALE)) };
}

void Enemy::draw_3d() {
	transform(d3ddev, loc_3d, D3DXVECTOR3{ roll_3d, yaw_3d, pitch_3d }, scl_3d);
	render_mesh->draw();
}

void Enemy::draw_2d() {
	tsprite->transform(loc_2d, rot_2d, scl_2d);
	tsprite->draw();

	// legit i have no idea what the heck this math is
	D3DXVECTOR2 vect1 = { engage_radius_inner / engage_circle->width, 
		engage_radius_inner / engage_circle->height };
	D3DXVECTOR2 vect2 = { (float)engage_circle->width * vect1.x * .25f, 
		(float)engage_circle->height * vect1.y * .25f };
	//print_vector_2d(vect1); 

	engage_circle->transform(loc_2d - vect2, 0.0f, vect1);
	engage_circle->draw();
}

void Enemy::update_3d() {
	update_physics();
}

void Enemy::update_2d() {
	D3DXVECTOR2 player_loc = flying_list[0]->loc_2d;
	if ((*RUNTIME_FLAGS) & IS_PLAYER_MOVING) {
		float speed = 1.5f;
		
		float angle = atan((player_loc.y - loc_2d.y) / (player_loc.x - loc_2d.x));

		if (loc_2d.x > player_loc.x) {
			loc_2d.x -= speed * cos(angle);
			loc_2d.y -= speed * sin(angle);

			rot_2d = angle - PI / 2;
		}
		else {
			loc_2d.x += speed * cos(angle);
			loc_2d.y += speed * sin(angle);

			rot_2d = angle + PI / 2;
		}
	}
	if (distance_2d(&player_loc, &loc_2d) < engage_radius_inner / 2) {
		//printf("engage!\n");
	}
}

void Enemy::Release() {
	printf("Begin Enemy release.\n");

	phys_body->Release();
}

Enemy::~Enemy() {
	printf("Enemy destructor called.\n");
}
