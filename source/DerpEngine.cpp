//
// DerpEngine.cpp: This project is a DirectX 9 engine made
// by Daniel "OH YEAH" Garcia and Ryan "McSwagmaster" Atkinson.
//
#include "stdafx.h"

// Prototype for the WindowProc message function
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
UINT UpdateThreadProc(void* param0);
void create_arrays(Entity**, Entity**);
void create_resources(LPDIRECT3DDEVICE9, LPD3DXSPRITE, TSprite**, Model**);

// Array that contains all keys. If value at a given index is 1, the key code with that index
// is down, if 0 is up.
int key_list[0xff];

int dCount = 25; //"Delay Count" used to delay input for switching modes so that swiching back and forth isn't too fast

// main: entry point for the program
int WINAPI wWinMain(_In_ HINSTANCE hInstance,
		_In_opt_ HINSTANCE hPrevInstance,
		_In_ LPWSTR    lpCmdLine,
		_In_ int       nCmdShow) {
	//_CrtSetBreakAlloc(204);

	// Struct that contains the window
	HWND hWnd;

	// Window information struct
	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	//wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = L"WindowClass";
	wc.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_AA));

	/*HANDLE icon = LoadImage(NULL, L"media//aaGun.bmp", IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE);

	if (icon == NULL) {
		printf("icon creation failed, error is %d\n", GetLastError());
	}*/
	//wc.hIcon = (HICON)icon;
	/*HICON h_icon = LoadIcon(NULL, IDB_BITMAP1);
	wc.hIcon = h_icon;*/
	RegisterClassEx(&wc);

	DWORD window_style = WS_EX_TOPMOST 
		| WS_POPUP 
		| WS_DLGFRAME 
		| WS_BORDER 
		| WS_CAPTION 
		| WS_EX_WINDOWEDGE 
		| WS_SYSMENU 
		| WS_MAXIMIZEBOX 
		| WS_MINIMIZEBOX;

	// Create window struct using window class data
	hWnd = CreateWindowEx(NULL,
		L"WindowClass",
		L"FlightDefence v0.0",
		window_style,
		SCREEN_X, SCREEN_Y,
		SCREEN_WIDTH, SCREEN_HEIGHT,
		NULL, NULL, NULL, NULL);

	// Show window, param1 must be 1
	ShowWindow(hWnd, 1);
	ShowCursor(1);

	uint8_t RUNTIME_FLAGS = IS_UPDATE_THREAD | DO_2D | IS_UPDATING;

	Entity **flying_list = new Entity*[MAX_FLYING_ENTITIES];
	Entity **ground_list = new Entity*[MAX_GROUND_ENTITIES];
	create_arrays(flying_list, ground_list);

	flying_list[0] = new Player(D3DXVECTOR2{ 100.0f, 400.0f }, key_list, &RUNTIME_FLAGS, hWnd, flying_list);
	flying_list[1] = new Enemy(D3DXVECTOR2{ 600.0f, 100.0f }, flying_list, ground_list, &RUNTIME_FLAGS);

	RENDERER_INFO r_wad{ &hWnd, flying_list, ground_list, &RUNTIME_FLAGS };
	Renderer ren(r_wad);

	UPDATE_INFO *u_wad = new UPDATE_INFO{ flying_list, ground_list, &RUNTIME_FLAGS };
	AfxBeginThread(UpdateThreadProc, (void*) u_wad);

	//MoveWindow(hWnd, 100, 100, 400, 400, FALSE);
	//SetWindowPos(hWnd, HWND_TOP, 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT, NULL);

	MSG msg;
	// Enter the message loop
	while (1) {
		// Peek, translate, then dispatch messages
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If the message is a quit (alt-f4) message, break out and end
		if (msg.message == WM_QUIT) {
			break;
		}

		if (GetAsyncKeyState('G')){
			if (dCount >= 1000) {
				RUNTIME_FLAGS ^= DO_2D;
				dCount = 0;
			}
		}

		if (GetAsyncKeyState(VK_ESCAPE)) {
			if (dCount >= 1000) {
				RUNTIME_FLAGS ^= IS_PAUSED;
				dCount = 0;
			}
		}
		
		if (GetAsyncKeyState('F')) {
			if (dCount >= 1000) {
				dCount = 0;
			}
		}

		dCount++;
		// Render a frame from the renderer

		if (RUNTIME_FLAGS & IS_UPDATING) {
			ren.render_frame();
		}
		else {
			//printf("not rendering\n");
		}
	}

	printf("Begin program exit.\n");
	RUNTIME_FLAGS &= ~IS_UPDATE_THREAD;

	for (int i = 0; i < MAX_GROUND_ENTITIES; i++) {
		if (ground_list[i]) {
			ground_list[i]->Release();
			delete ground_list[i];
		}
	}
	for (int i = 0; i < MAX_FLYING_ENTITIES; i++) {
		if (flying_list[i]) {
			flying_list[i]->Release();
			delete flying_list[i];
		}
	}

	while (!(RUNTIME_FLAGS & IS_UPDATER_DEAD));
	delete[] flying_list;
	delete[] ground_list;

	return msg.wParam;
}

UINT UpdateThreadProc(void *param0) {
	UPDATE_INFO *info = (UPDATE_INFO*)param0;

	if (!info) {
		printf("Object is null, breaking out of thread creation.\n");
		AfxEndThread(1);
	}

	while (info && (*(info->RUNTIME_FLAGS)) & IS_UPDATE_THREAD) {
		if (!(*(info->RUNTIME_FLAGS) & IS_PAUSED)) {
#pragma warning(suppress: 28159)
			long time = GetTickCount();

			*(info->RUNTIME_FLAGS) &= ~IS_UPDATING;

			if (*(info->RUNTIME_FLAGS) & DO_2D) {
				//printf("begin player updating 2d\n");
				for (int i = 0; i < MAX_GROUND_ENTITIES; i++) {
					if (info->ground_list[i]) {
						info->ground_list[i]->update_2d();
					}
				}
				for (int i = 0; i < MAX_FLYING_ENTITIES; i++) {
					if (info->flying_list[i]) {
						info->flying_list[i]->update_2d();
					}
				}
			}
			else {
				//printf("begin player updating 3d\n");
				for (int i = 0; i < MAX_GROUND_ENTITIES; i++) {
					if (info->ground_list[i]) {
						info->ground_list[i]->update_3d();
					}
				}
				for (int i = 0; i < MAX_FLYING_ENTITIES; i++) {
					if (info->flying_list[i]) {
						info->flying_list[i]->update_3d();
					}
				}
			}
			*(info->RUNTIME_FLAGS) |= IS_UPDATING;

#pragma warning(suppress: 28159)
			long diff = GetTickCount() - time;
			if (diff < UPDATE_MILLIS) {
				Sleep(UPDATE_MILLIS - diff);
				//printf("Slept for %d milliseconds\n", UPDATE_MILLIS - diff);
			}
		}
	}

	*(info->RUNTIME_FLAGS) |= IS_UPDATER_DEAD;
	delete info;
	AfxEndThread(0);
	return 0;
}

// WindowProc: handles events sent to the window
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	// Switch block for all messages
	switch (message) {
		// If the message is a "destroy" (alt + f4)  it will post a quit message to
		// delete the window
		case WM_DESTROY: {
			PostQuitMessage(0);
			return 0;
		} break;
		// If a key has been pressed, sets the index of the key_list at the keycode to 1 (true)
		case WM_KEYDOWN: {
			key_list[wParam] = 1;
		} break;
		// If a key has been released, sets the index to 0 (false)
		case WM_KEYUP: {
			key_list[wParam] = 0;
		}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

void create_arrays(Entity **flying_list, Entity **ground_list) {
	ZeroMemory(flying_list, sizeof(Entity*) * MAX_FLYING_ENTITIES);
	ZeroMemory(ground_list, sizeof(Entity*) * MAX_GROUND_ENTITIES);
}
