#include "stdafx.h"

#ifndef ENTITY_H_
#define ENTITY_H_

class Entity {
public:
	Entity() {}
	~Entity() {}

	D3DXVECTOR3 loc_3d{ 0.0f, 0.0f, 0.0f };
	D3DXVECTOR3 scl_3d{ 1.0f, 1.0f, 1.0f };
	float roll_3d, yaw_3d, pitch_3d;

	D3DXVECTOR2 loc_2d{};
	D3DXVECTOR2 scl_2d{};
	float rot_2d = 0.0f;
	
	LPDIRECT3DDEVICE9 d3ddev;
	LPD3DXSPRITE spritedev;

	Model *render_mesh = 0;
	TSprite *tsprite = 0;

	RECT rect;

	enum FACTION_TYPE faction;
	Entity **flying_list;
	Entity **ground_list;
	uint8_t *RUNTIME_FLAGS;

public:
	virtual void draw_3d() = 0;
	virtual void draw_2d() = 0;
	virtual void update_3d() = 0;
	virtual void update_2d() = 0;
	virtual void Release() = 0;
	virtual void init_mesh(LPDIRECT3DDEVICE9, Model**) = 0;
	virtual void init_sprite(LPDIRECT3DDEVICE9, LPD3DXSPRITE, TSprite**) = 0;

};

#endif // ENTITY_H_
